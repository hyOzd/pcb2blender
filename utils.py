# Generic functions and classes for pcb2blender

from math import cos,sin,tan,atan,pi,sqrt,pow,fabs,radians
import copy

class Point:

    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)

    @classmethod
    def fromPolar(cls,r,a):
        return cls(r*cos(a),r*sin(a))

    @classmethod
    def midOf(cls,p1,p2):
        return cls((p1.x+p2.x)/2,(p1.y+p2.y)/2)

    def distanceTo(self,other):
        return sqrt(pow(self.x-other.x,2)+pow(self.y-other.y,2))

    def angleTo(self,other):
        dx = other.x-self.x
        dy = other.y-self.y

        if dx==0:
            return pi/2 if dy>=0 else -pi/2
        else:
            a = atan(dy/dx)
            if dx<0: a+= pi
            return a

    """Rotate point around origin"""
    def rotated(self,rot):
        c = cos(rot)
        d = sin(rot)
        a = self.x*c-self.y*d
        b = self.x*d+self.y*c
        return Point(a,b)

    def __eq__(self,other):
        if isinstance(other,Point):
            return (self.x==other.x)and(self.y==other.y)
        else:
            return NotImplemented

    def __add__(self,other):
        return Point(self.x+other.x,self.y+other.y)

    def __sub__(self,other):
        return Point(self.x-other.x,self.y-other.y)

    def __neg__(self):
        return Point(-(self.x),-(self.y))

    def __pos__(self):
        return Point(self.x,self.y)

    def __iadd__(self,other):
        self.x += other.x
        self.y += other.y
        return self

    def __isub__(self,other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __mul__(self, other):
        return Point(self.x*other,self.y*other)

    def __imul__(self, other):
        self.x *= other
        self.y *= other
        return self

    def __complex__(self):
        return self.x+1j*self.y

    def __repr__(self):
        return '('+str(self.x)+' '+str(self.y)+')'

class Line:

    def __init__(self,sx,sy,ex,ey,curve=0.0):
        self.start = Point(sx,sy)
        self.end = Point(ex,ey)
        self.curve = curve

    def length(self):
        if self.curve == 0:
            return self.start.distanceTo(self.end)
        else:
            return self.getCurveRadius()*fabs(self.curve)

    def getReversed(self):
        return Line(self.end.x,self.end.y,
                    self.start.x,self.start.y,
                    self.curve)

    def getCurveRadius(self):
        if self.curve == 0:     # no curve here TODO: maybe exception
            return 0
        else:
            d = self.start.distanceTo(self.end)
            return fabs((d/2)/sin(fabs(self.curve)/2))

    def getCurveCenter(self):
        if self.curve == 0:     # no curve here TODO: maybe exception
            return Point.midOf(self.start,self.end)
        else:
            aa = self.curve     # arc angle
            if aa<0:
                A = self.end
                B = self.start
                aa = -aa
            else:
                A = self.start
                B = self.end

            M = Point.midOf(A,B)
            a = A.angleTo(B)
            d = A.distanceTo(M)/tan(aa/2)   # distance from M to center
            C = M+Point.fromPolar(d,a+pi/2)     # move to center from M
            return C

    # @param ns Number of segments
    def getCurveSegmented(self,ns=0):
        if self.curve == 0: # no curve
            return [self.start,self.end]
        else:
            if ns == 0:
                # watch out curve length is in mm (blender units are cm)
                ns = abs(round(5*self.getCurveLength()))
            rd = self.getCurveRadius()
            C = self.getCurveCenter()
            rots = C.angleTo(self.start)    # rotation start

            segs = [C+Point.fromPolar(rd,self.curve*i/ns+rots)
                    for i in range(1,ns)]

            segs.insert(0,self.start)
            segs.append(self.end)

            return segs

    def getCurveLength(self):
        if self.curve == 0:
            return 0
        else:
            return self.curve*pi

    def __repr__(self):
        if self.curve == 0:
            return str(self.start)+' -> '+str(self.end)
        else:
            return str(self.start)+' ~'+str(self.curve)+'> '+str(self.end)

class Hole:

    #@param drill Drill diameter
    def __init__(self,x,y,drill):
        self.pos = Point(x,y)
        self.drill = float(drill)
        self.layers = ("TOP","BOTTOM")

    def __repr__(self):
        return "Hole at: "+str(self.pos)+" d: "+str(self.drill)+"mm"

"""Sorts wires as a path."""
def getWiresInPath(wires):
    path = [wires[0]]
    del wires[0]

    while len(wires)>0 :
        for i,wire in enumerate(wires):
            if wire.start == path[-1].end :
                path.append(wire)
                del wires[i]
                break
            elif wire.end == path[-1].end :
                path.append(wire.getReversed())
                del wires[i]
                break
        else:
            print("Board outline may have problems!")
            break

    if path[0].start != path[-1].end :
        print("Board outline must be a closed shape!")
        return None
    else:
        return path

"""Returns a list of points from a list of lines"""
def getPointsFromPath(linePath):
    path = []
    for line in linePath:
        if line.curve == 0:
            path.append(line.start)
        else:
            path.extend(line.getCurveSegmented()[0:-1])

    path.append(line.end)

    return path
